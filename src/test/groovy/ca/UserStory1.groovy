package ca
import static org.junit.Assert.assertTrue

import org.junit.Test
import org.mockito.MockitoAnnotations

import asset.Asset
import bot.Bot
import bot.BotListener
import mock.MarketFake
import operation.OperationType
import rules.AssetRule
import spock.lang.Specification
import wallet.Wallet

class UserStory1 extends Specification {
	def  Bot bot;
	def  Asset asset;
	def  List<AssetRule> activeRules;
	
	def setup()
	{
		// before
		MockitoAnnotations.openMocks(this);
		this.asset = new Asset("BITCOIN","BTC");
		this.activeRules= new ArrayList<AssetRule>();
	}
	
	def "El proceso de generación de Orden de Compra se intenta ejecutar sin reglas definidas"(){
		given: "una nueva intancia de bot"
			createBot();
			
		when: "se ejecuta una operacion de Orden de Compra"
			bot.operate();
		
		then: "No se generan cambios, no hay reglas definidas."
			bot.getRules().size() == 0;

	}
	
	def "Con 1 regla declarada, se ejecutar una Orden de Compra con el valor del activo por debajo del porcentaje declarado, en la regla"(){
		given: "una sola regla para la operacion de compra de 1 bitcoin en binance en el listado de reglas a ejecutar"
			addRule(OperationType.BUY);
			
		and: "creando una instancia de bot"
			createBot();
			
		and: "obteniendo el valor del bitcoin en el mercado"
			MarketFake fake = (MarketFake) bot.getMarket();
			fake.setPrice(94.0);
			
		when: "se ejecuta una operacion de Orden de Compra"
			bot.operate();
		
		then: "se genera una orden de compra de 1 bitcoin a 94 pesos y se elemina la regla ejecutada de la lista"
			bot.getRules().size() == 0;
	}
	

	def "Con 1 regla declarada, se ejecutar una Orden de Compra con el valor del activo por encima del porcentaje declarado, en la regla"(){
		given: "una sola regla para la operacion de compra de 1 bitcoin en binance en el listado de reglas a ejecutar"
			addRule(OperationType.BUY);
			
		and: "creando una instancia de bot"
			createBot();
			
		and: "obteniendo el valor del bitcoin en el mercado"
			MarketFake fake = (MarketFake) bot.getMarket();
			fake.setPrice(96.0);
			
		when: "se ejecuta una operacion de Orden de Compra"
			bot.operate();
		
		then: "No se generan cambios, la regla permanece en la lista para ser ejecutada en otro momento"
			bot.getRules().size() == 1;
			bot.getRules().get(0).getOperationType() == OperationType.BUY;
	}
	
	def "Con 2 reglas declaradas, se ejecutar una Orden de Compra con el valor del activo por debajo del porcentaje declarado en una regla"(){
		given: "una de las reglas para la operacion de compra de 1 bitcoin en binance en el listado de reglas a ejecutar"
			addRule(OperationType.BUY);
			addRule(OperationType.SELL);
			
		and: "creando una instancia de bot"
			createBot();
			
		and: "obteniendo el valor del bitcoin en el mercado"
			MarketFake fake = (MarketFake) bot.getMarket();
			fake.setPrice(94.0);
			
		when: "se ejecuta una operacion de Orden de Compra"
			bot.operate();
		
		then: "se genera una orden de compra de 1 bitcoin a 94 pesos y se elemina la regla ejecutada de la lista"
			bot.getRules().size() == 1;
			bot.getRules().get(0).getOperationType() == OperationType.SELL;
	}
	
	def "Con 2 reglas declaradas, se ejecutar una Orden de Compra con el valor del activo por encima del porcentaje declarado en una regla"(){
		given: "una de las reglas para la operacion de compra de 1 bitcoin en binance en el listado de reglas a ejecutar"
			addRule(OperationType.BUY);
			addRule(OperationType.SELL);
			
		and: "creando una instancia de bot"
			createBot();
			
		and: "obteniendo el valor del bitcoin en el mercado"
			MarketFake fake = (MarketFake) bot.getMarket();
			fake.setPrice(96.0);
			
		when: "se ejecuta una operacion de Orden de Compra"
			bot.operate();
		
		then: "No se generan cambios, la regla de operacion de compra permanece en la lista para ser ejecutada en otro momento"
			bot.getRules().size() == 2;
	}
	
//	@Test
//	public void testOperateWithTwoRulesPriceNotChanged() {
//		addRule(OperationType.BUY);
//		addRule(OperationType.SELL);
//		createBot();
//		bot.operate();
//		assertTrue(bot.getRules().size() == 2);
//	}
	
	def createBot() {
		Wallet wallet = new Wallet();
		wallet.addAmount(500);
		bot= new Bot(wallet, activeRules, 0, new MarketFake());
		@SuppressWarnings("unused")
		BotListener botListener = new BotListener(bot);
	}
	
	def addRule(OperationType type)
	{
		activeRules.add(new AssetRule(5, asset,1, type, 100));
	}
}
