package testUnitarios;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import asset.Asset;
import bot.BotSetupDTO;
import operation.OperationType;
import rules.AssetRule;

public class BotSetupDTOTest {
	private BotSetupDTO botSetupDTO;
	private double amount;
	private int loopTime;
	private List<AssetRule> rules;
	private Asset asset;
	private String _path;
	private String _package;
	
	@Before
	public void init() {
		asset = new Asset("BITCOIN","BTC");
		amount = 100;
		_path = "path";
		_package = "package";
		rules = new ArrayList<>();
		addRule(OperationType.BUY);
		loopTime = 0;
		botSetupDTO = new BotSetupDTO(loopTime, amount, rules, _path, _package);
	}
	
	@Test
	public void testBotSetupGetLoopTime() {
		assertTrue(botSetupDTO.getLoopTime() == 0);
	}
	
	@Test
	public void testBotSetupGetRules() {
		assertTrue(botSetupDTO.getRules().equals(rules));
	}
	
	@Test
	public void testBotSetupGetWallet() {
		assertTrue(botSetupDTO.getAmount()==100);
	}
	
	@Test
	public void testBotSetupGetPath() {
		assertTrue(botSetupDTO.getPath().equals(_path));
	}
	
	@Test
	public void testBotSetupGetPackage() {
		assertTrue(botSetupDTO.getPackage().equals(_package));
	}

	@Test
	public void testBotSetupSetLoopTime() {
		botSetupDTO.setLoopTime(1);
		assertTrue(botSetupDTO.getLoopTime() == 1);
	}
	
	@Test
	public void testBotSetupSetPath() {
		botSetupDTO.setPath("other");
		assertTrue(botSetupDTO.getPath().equals("other"));
	}
	
	@Test
	public void testBotSetupSetPackage() {
		botSetupDTO.setPackage("other");
		assertTrue(botSetupDTO.getPackage().equals("other"));
	}
	
	@Test
	public void testBotSetupSetRules() {
		addRule(OperationType.SELL);
		botSetupDTO.setRules(rules);
		assertTrue(botSetupDTO.getRules().equals(rules));
	}
	
	@Test
	public void testBotSetupSetWallet() {
		double amount = 200;
		botSetupDTO.setAmount(amount);
		assertTrue(botSetupDTO.getAmount()==200);
	}
	
	
	private void addRule(OperationType type)
	{
		rules.add(new AssetRule(1, asset,1, type, 100));
	}
}
