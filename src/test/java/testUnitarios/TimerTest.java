package testUnitarios;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import timer.Timer;

public class TimerTest {
	private Timer timer;
	@Before
	public void init() {
		timer = new Timer(0);
	}
	
	@Test
	public void testTimerIsReadyOk() {
		timer.start();
		assertTrue(timer.isReady()==true);
	}
	@Test
	public void testTimerIsReadyFalse() {
		timer = new Timer(10);
		timer.start();
		assertTrue(timer.isReady()==false);
	}
}
