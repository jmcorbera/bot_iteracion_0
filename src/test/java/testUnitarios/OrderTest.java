package testUnitarios;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import asset.Asset;
import operation.OperationType;
import operation.Order;

public class OrderTest {
	private Order order;
	private Date orderDate;
	private Asset asset;

	@Before
	public void init() {
		orderDate = new Date();
		asset = new Asset("BITCOIN","BTC");
		order = new Order(10, orderDate, 1, asset, OperationType.BUY);
	}
	
	@Test
	public void testOrderGetAssetOk() {
		assertTrue(order.getAsset().equals(asset));
	}
	
	@Test
	public void testOrderGetPriceOk() {
		assertTrue(order.getPrice()==10);
	}
	
	@Test
	public void testOrderGetOrderDateOk() {
		assertTrue(order.getDate().equals(orderDate));
	}
	
	@Test
	public void testOrderGetOperationTypeOk() {
		assertTrue(order.getType()==OperationType.BUY);
	}
	
	@Test
	public void testOrderGetQuantityOk() {
		assertTrue(order.getQuantity()==1);
	}
	
}
