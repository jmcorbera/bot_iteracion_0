package mock;

import market.Market;
import operation.Order;

public class MarketFake implements Market{
	private double price = 100;

	@Override
	public double getPrice(String symbol) {
		return price;
	}

	@Override
	public String getName() {
		return "BINANCE";
	}

	@Override
	public String placeOrder(Order order) {
		return "abc123";
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
