package market;
import operation.Order;

public interface Market{
	public double getPrice(String symbol);
	public String getName();
	public String placeOrder(Order order);
}
