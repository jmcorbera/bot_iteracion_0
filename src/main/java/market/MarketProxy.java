package market;
import operation.Order;

public class MarketProxy implements Market{
	private Market market;

	public MarketProxy(Market market) {
		this.market = market;
	}

	@Override
	public double getPrice(String symbol) {
		return market.getPrice(symbol);
	}

	@Override
	public String getName() {
		return market.getName();
	}

	@Override
	public String placeOrder(Order order) {
		return market.placeOrder(order);
	}

}
