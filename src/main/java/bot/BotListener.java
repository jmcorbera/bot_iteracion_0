package bot;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class BotListener implements PropertyChangeListener{
	private Bot bot;
	
	public BotListener(Bot bot)
	{
		this.bot = bot;
		bot.addPropertyChangeListener(this);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent change) {
		bot.removeRule((int) change.getOldValue());
	}

}
