package operation;

import asset.Asset;
import java.util.Date;

public class Order {
    private double price;
    private Date date;
    private double quantity;
    private Asset asset;
    private OperationType type;

    public Order(double price, Date date, double quantity, Asset asset, OperationType type) {
        super();
        this.price = price;
        this.date = date;
        this.quantity = quantity;
        this.asset = asset;
        this.type = type;
    }
    public double getPrice() {
        return price;
    }

    public Date getDate() {
        return date;
    }

    public double getQuantity() {
        return quantity;
    }

    public Asset getAsset() {
        return asset;
    }

    public OperationType getType() {
        return type;
    }
}
