package timer;

public class Timer{
    private long startTime;
    private int waitingTime;

    public Timer(int waitingTime){
        startTime = 0;
        this.waitingTime = waitingTime * 60000;
    }

    public boolean isReady(){
        long actualTime = System.currentTimeMillis();
        if(actualTime - startTime >= waitingTime){
            startTime = actualTime;
            return true;
        }
        else{
            return false;
        }
    }

    public void start(){
        startTime = System.currentTimeMillis();
    }

}
